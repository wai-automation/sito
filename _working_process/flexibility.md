---
process_title: FLEXIBILITY
process_short_desc: >-
  <!--StartFragment-->


  We transform the traditional Customer-Supplier relationship into a real technological partnership: **we support our customers in solving traditional problems with unconventional methods.**


  <!--EndFragment-->
---
