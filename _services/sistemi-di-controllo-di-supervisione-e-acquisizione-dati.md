---
title: SUPERVISION CONTROL AND DATA ACQUISITION SYSTEMS
icon: fa fa-bar-chart
thumbnail: /assets/images/uploads/system.jpg
---
<!--StartFragment-->

WAI A﻿utomation designs and develops ad hoc software, for monitoring, control and tracking. 

The vision software can be integrated into the touch screen panels on board the machine, or to a PC for dedicated stations. The software is designed according to customer specifications and offers multiple functions, such as storing and archiving measures, events, alarms and process information. 

Data storage can be done locally in a file in the PC system, in a database via the intranet communication network (LAN), or database to another remote location, via geographic communication network (WAN). The process analyses data, extracts significant data useful for more accurate analysis of the system, displaying them in graphic media, and possibly communicating them to the corporate information system. 

According to customer requirements, the supervision system is configured to perform preset operations, the occurrence of specific events or processes in the system.

<!--EndFragment-->