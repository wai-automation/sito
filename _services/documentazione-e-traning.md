---
title: DOCUMENTATION AND TRAINING
icon: fa fa-leanpub
thumbnail: /assets/images/uploads/documentazione.jpg
---
<!--StartFragment-->

W﻿ai Automation furnishes necessary documentation in support of its projects and of its work, such as technical specifications, wiring diagrams and documentation. 

Decades of experience has provided W﻿ai Automation a great deal of specific knowledge used to complete documentation of systems, which is essential when installing upgrade changes. 

The right documentation will allow the customer to properly maintain the system in addition to allowing rapid intervention without errors and loss of time.

<!--EndFragment-->