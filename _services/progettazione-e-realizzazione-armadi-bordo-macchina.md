---
title: DESIGN AND CONSTRUCTION OF ELECTRICAL CABINET
icon: fa fa-plug
thumbnail: /assets/images/uploads/quadri.jpeg
---
<!--StartFragment-->

By partnering with companies that deal with electrical design, realization of Electrical Cabinet and the machine equipment, W﻿ai Automationis able to offer customers a complete service, providing more than the automation of software including hardware and cabling, with added value to have the maximum customization of their systems and ensuring full compatibility between software and hardware.

<!--EndFragment-->