---
title: INSTALLATION AND SET UP
icon: fa fa-wrench
thumbnail: /assets/images/uploads/installation.jpg
---
<!--StartFragment-->

W﻿ai Automation is already a supplier to the global automotive industry. We provide our services in Europe, the U.S., China and Latin America, where we operate in the field by installing and configuring setup to commence operation of complex automation systems. Our software is now installed in companies such as  FCA, PSA, Volkswagen, Audi, Daimler, Haier, Candy and many others.

<!--EndFragment-->