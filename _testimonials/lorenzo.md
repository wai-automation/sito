---
thumbnail: /assets/images/uploads/uomo.jpg
thumbnail_alt_text: Lorenzo Gnocchi, Senior Automation Engineer
testimonial_name: Lorenzo
testimonial_title: Senior Automation Engineer
message: La determinazione di oggi crea il successo di domani
---
