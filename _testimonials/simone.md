---
thumbnail: /assets/images/uploads/simone-03.png
thumbnail_alt_text: Simone Di Gilio, Junior Software Developer
testimonial_name: Simone
testimonial_title: Junior Software Developer
message: Siamo tutti apprendisti in un mestiere dove non si diventa mai maestri
---
