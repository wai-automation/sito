---
thumbnail: /assets/images/uploads/mattia_small.jpg
thumbnail_alt_text: Mattia Ducci, sviluppo e manutenzione del sito web
testimonial_name: Mattia
testimonial_title: Programmatore web e mobile
message: E' veramente un onore far parte di questo sito!
---
