---
thumbnail: /assets/images/uploads/marco-02.jpeg
thumbnail_alt_text: Marco Massardi, Senior Software Developer
testimonial_name: Marco
testimonial_title: Senior Software Developer
message: L’uomo che sposta una montagna comincia portando via piccole pietre
---
