---
header_text: Software design and development for industrial automation
description: Software design and development for industrial automation
bg_color: rgba(68, 3, 80, 0.58)
bg_image: /assets/images/uploads/csm_powertrain_piaautomation_automotive_mobility_2_91409a103e.jpg
text_color: "#ffffff"
learn_more_link_destination: https://wai-automation.netlify.app/services/design-and-development-of-software-for-industrial-automation
---
