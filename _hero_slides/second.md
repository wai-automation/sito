---
header_text: Installation and commissioning
description: Installation and commissioning
bg_color: "#DAEB47"
bg_image: /assets/images/uploads/csm_eol_powertrain_piaautomation-slider_6d1d3256d1.jpg
text_color: "#090909"
learn_more_link_destination: https://wai-automation.netlify.app/services/installazione-e-messa-in-funzione
---
