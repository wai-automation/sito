---
title: CAVITY LINE OF OVENS (Transfer)
thumbnail: /assets/images/uploads/cooking5_.jpg
filters:
  - plc
  - siemens
  - robot
brands:
  - Siemens
ides:
  - TiaPortal V17
  - Kuka
skills:
  - software development
  - commissioning
  - project management
languages:
  - KOP
  - SCL
reference_date: 2023-11-19T13:35:59.435Z
customer: Vestel (Scamm)
---
