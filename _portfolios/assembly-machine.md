---
title: ASSEMBLY MACHINE
thumbnail: /assets/images/uploads/families-069.jpg
filters:
  - plc
  - siemens
brands:
  - Siemens
ides:
  - TiaPortal15
skills:
  - software development
  - commissioning
  - project management
languages:
  - KOP
  - SCL
reference_date: 2023-02-28T21:17:53.217Z
customer: SCANIA
---
