---
title: CAVITY LINE OF OVENS (Robot Kuka)
thumbnail: /assets/images/uploads/whatsapp-image-2023-06-30-at-17.58.34.jpeg
filters:
  - plc
  - siemens
  - robot
brands:
  - Siemens
  - Kuika
ides:
  - Tia Portal 18
  - Kuka
skills:
  - software development
  - commissioning
  - project management
languages:
  - KOP
  - SCL
reference_date: 2023-07-31T10:28:45.913Z
customer: Haier (Candy)
---
