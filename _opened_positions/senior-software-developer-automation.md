---
title: Senior Software Developer (Automation)
details: >-
  Il Programmatore Senior si occuperà dello sviluppo di progetti di automazione
  (motion) e controllo processi, all'interno dell'Ufficio Tecnico, trasferte in
  Italia e Estero.


  Si richiede esperienza pregressa nello sviluppo e avviamento di impianti industriali.

  Necessaria la conoscenza linguaggi di programmazione PLC, RTU e HMI/SCADA (Siemens, Allen Bradley, Omron o altri).


  Necessaria una discreta conoscenza dell'inglese, apprezzata di altre lingue.


  Contratto di lavoro: Tempo indeterminato
---
